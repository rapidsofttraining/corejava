package corejava;

public class Car extends Vehicle {
	
	public void setModel()
	{
		System.out.println("In Car, setModel method");
	}

	public void setPassengers()
	{
		System.out.println("In Car, setPassengers method");
	}
}
