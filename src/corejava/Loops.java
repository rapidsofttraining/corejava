package corejava;

public class Loops {
	
	/*
	 * We will learn enhanced for loop
	 */
	
	// if else, &&,  ||
	// Relational > < = >=
	
	// loops repeating the same task for n number of times or till the condition is met.
	
	public static void main(String[] args) {
		
//		for(int i =1; i < 10; i ++)
//		{
//			System.out.println("I am in for loop count is "  + i);
//		}
		int j =0;
		// while(if the condition is true then execute the while loop)
		
		
		while (j < 10)
		{
			System.out.println("I am in while loop count is "  + j);
			j++;
		}
		
		
		
	}
	
	
	
	
	
	

}
