package corejava;

public class AddStrings {

	public static void main(String[] args) {
		System.out.println(getFullName("Deepa", "Malgan"));
		String name = "   Deepa Malgan   ";
		System.out.println(name.length());
		String afterTrim = testTrim(name);
		System.out.println(afterTrim.length());
	}

	private static String getFullName(String fName, String lName) {
		return fName + " " + lName;
	}

	private static String testTrim(String name) {

		return name.trim();

	}

}