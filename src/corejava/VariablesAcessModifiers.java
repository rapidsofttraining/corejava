package corejava;

import java.util.*;

public class VariablesAcessModifiers {
                 //return type - void; method name is main, string[] is 
	             //arugments or parameters
	
	public static void main(String[] args) {
		// declaring variables
		int age;
		String name;
		// assigning values to variables
		age = 14;
		name = "sri";
		// int, string, bolean, float, double
		if (age <= 10) {
			System.out.println("Elementary School");
		}
		// > < = relational operators  && logical operator  || or
		else if (age > 10 && age <= 14) {
			System.out.println("Middle school");
		} else {
			System.out.println("High school and above");

		}
		//System.out.println("I am in the end of main");
		int sum = addNumbers(10,20);
		System.out.println(sum);
		
		
	}

	// control shift - F
	public static void testMethod() {
		System.out.println("In testMethod");
	}
	
	private static int addNumbers(int number1, int number2) {
		int sumoftwonumbers = number1 + number2;
		System.out.println("In addNumbers");
		System.out.println("addNumbers" + sumoftwonumbers);

		return sumoftwonumbers;
		
		
	}

}
