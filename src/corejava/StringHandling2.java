package corejava;

public class StringHandling2 {

	public static void main(String[] args) {
		String cityName1 = "Ashburqqqqqqqqq1111";
		String cityName2 = "Ashb";

		System.out.println(cityName1.length());
		System.out.println(cityName2.length());
		
		int compare2 = cityName1.compareTo(cityName2);
		System.out.println(compare2);
		
		String stateName1 = "Virginia1";
		String stateName2 = "virginia1";
		
		System.out.println(stateName1.equals(stateName2));
		System.out.println(stateName1.equalsIgnoreCase(stateName2));
        // "Expected vs Actual"

	}
}
