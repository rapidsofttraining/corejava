package corejava;

import java.util.ArrayList;

public class Collections {
	// List, Map Set, Queue are different types of collections.
	//ArrayList
	//HashMap
	
	String companyName = "RapidSoft";
	String companyName1 = "Xyz Corp";

	public static void main(String[] args) {
		 ArrayList<String> companyNames = new ArrayList<String>();
		 
		 companyNames.add("RapidSoft");
		 companyNames.add("KPMG");
		 companyNames.add("Sri Soft");
		 companyNames.add("DC Corp");
		 companyNames.add("DC Corp");
		 companyNames.add("DC Corp1");
		 companyNames.add("DC Corp2");
		 companyNames.add("DC Corp3");
		 companyNames.add("DC Corp4");

		 

//		 System.out.println(companyNames.get(0));
//		 System.out.println(companyNames.get(1));
//		 System.out.println(companyNames.get(2));
//		 System.out.println(companyNames.get(3));
//		 System.out.println(companyNames.get(4));
		 
//		 System.out.println(companyNames.size());
		 
//		 for(int i=0; i<companyNames.size(); i++)
//		 {
//			 System.out.println(companyNames.get(i));
// 		 }
		 
//		 for(String company:companyNames)
//		 {
//			 System.out.println(company);
//		 }
		 

	// ArrayList uses index. And keeps the order.
	//	 
	
		 Employee emp1 = new Employee();
		 emp1.setName("Anand");
		 emp1.setSal(2000);
		 emp1.setDepartment("IT");
		 
		 
		 Employee emp2 = new Employee();
		 emp2.setName("Deepa");
		 
		 Employee emp3 = new Employee();
		 emp3.setName("Ram");
		 
		 ArrayList<Employee> Employees = new ArrayList<Employee>();
		 Employees.add(emp1);
		 Employees.add(emp2);
		 Employees.add(emp3);
		 
		 for(Employee emp:Employees)
		 {
			 System.out.println(emp.getName());
			 
		 }
		 
		 
		 
		 
		 
		 
		 
	}
	
	
	  
	
	   
	
	
	
	
	
	
	
	

}
