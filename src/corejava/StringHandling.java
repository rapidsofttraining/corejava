package corejava;

public class StringHandling {
	
	public static void main(String[] args) {
		// concat
		// substr
		// len
		
		String companyName = "RapidSoft";
		String trainingName = companyName.concat(" Training");
		System.out.println(trainingName);
		
		String companyNamePlus = "RapidSoft";
		String trainingNamePlus = companyNamePlus + " Training";
		System.out.println(trainingNamePlus);
		//RapidSoft Training
		// Reservation Number: 1JKLMQ
		
		
		String newSub = trainingName.substring(3);
		System.out.println(newSub);
		
		String newSub1 = trainingName.substring(10,18);
		System.out.println(newSub1);
		
		String message =  "Than1k you Anand,Reservation Number: 1JKLMQ";
		int lengthOfmessage = message.length();
		System.out.println("lengthOfmessage " + lengthOfmessage);
		//42 - 6  = 36 - start
		System.out.println(message.substring(lengthOfmessage-6));
		
		if (message.contains("Thank you"))
		{
			System.out.println("Yes message contains thank you");
		}
		else
		{
			System.out.println(" message does not contains thank you");

		}
	

		
		
		
		
		
		
	}

}
