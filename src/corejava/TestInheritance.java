package corejava;

public class TestInheritance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car mycar = new Car();
		
		mycar.start();
		//mycar.thisisHowtoStart();  this will complain since method is private
		mycar.moveForward();
		mycar.setModel();
		mycar.setPassengers();
		mycar.stop();
		
	}

}
