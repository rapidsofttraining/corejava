package corejava;

public class AllLoops {

	   public static void main(String args[]) {
	      int x = 20;

	      while( x < 30 ) {
	         System.out.print("value of x : " + x );
	         x++;
	         System.out.print("\n");
	      }
	   }
	}

