package corejava;

public class Dog extends Animal  {
	
	public void move()
	{
		System.out.println("Dogs can walk and run");
	}

	public void smell()
	{
		System.out.println("Dogs can smell ");
	}
}
