package corejava;

public class Vehicle {
	
	public void start()
	{
		System.out.println("In Vehicle, start method");
		thisisHowtoStart();
	}
	
	private void thisisHowtoStart()
	{
		System.out.println("start alternator");
		System.out.println("spark cylinders");
		System.out.println("convert linear motion to circular");
	}
	
	public void moveForward()
	{
		System.out.println("In Vehicle, moveForward method");

	}
	
	public void setPassengers()
	{
		System.out.println("In Vehicle, setPassengers method");
	}
	
	public void stop()
	{
		System.out.println("In Vehicle, stop method");

	}
	

}
