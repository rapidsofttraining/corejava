package corejava;

import java.util.ArrayList;

public class ArrayMain {

	public static void main(String[] args) {
 
		CarArray carObject = new CarArray();
		carObject.setName("Honda");
		carObject.setColor("Grey");
		carObject.setPrice(30000);

		CarArray carObject1 = new CarArray();
		carObject1.setName("Audi");
		carObject1.setColor("White");
		carObject1.setPrice(40000);

		CarArray carObject2 = new CarArray();
		carObject2.setName("Toyota");
		carObject2.setColor("4Runner");
		carObject2.setPrice(40000);

		
		ArrayList<CarArray> Cars = new ArrayList<CarArray>();

		Cars.add(carObject);
		Cars.add(carObject1);
		Cars.add(carObject2);
		Cars.add(null);
		
		
		
		for (CarArray Car : Cars) {
			System.out.println("---- " + Car.getName());

		}
		processCars(Cars);
	
	}
	
	public static void processCars(ArrayList<CarArray> list)
	{
		for (CarArray Car : list) {
			System.out.println("---- " + Car.getName());

		}
		
	}


}